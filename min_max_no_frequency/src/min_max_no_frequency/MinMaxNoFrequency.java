package min_max_no_frequency;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MinMaxNoFrequency {

	public static <Key, Value> Key getKey(Map<Key, Value> map, Value value) {

		for (Entry<Key, Value> entry : map.entrySet())

			if (value.equals(entry.getValue()))

				return entry.getKey();

		return null;
	}

	public static void getMinAndMaxKey(Map<Integer, Integer> map) {

		Integer min = Collections.min(map.values());
		Integer max = Collections.max(map.values());

		Integer minKey = getKey(map, min);
		Integer maxKey = getKey(map, max);

		for (Entry<Integer, Integer> entry : map.entrySet()) {

			if (min.equals(entry.getValue()) && minKey > entry.getKey())
				minKey = entry.getKey();

			if (max.equals(entry.getValue()) && maxKey < entry.getKey())
				maxKey = entry.getKey();

		}
		System.out.println("Number with equal frequency choose smallest number =  " + minKey);
		System.out.println("Number with equal frequency or grater frequency choose grater number === " + maxKey);

	}

	public static void main(String[] args) {

		int ar[] = { -2, -2, 1, 2, 3, 3, 2, 2, 1, 3, 45, 45, 45 };

		HashMap<Integer, Integer> h = new HashMap<>();

		for (Integer i : ar) {

			h.computeIfPresent(i, (key, n) -> h.get(key) + 1);
			h.computeIfAbsent(i, key -> 1);
		}

		getMinAndMaxKey(h);
		System.out.println(h);

	}

}