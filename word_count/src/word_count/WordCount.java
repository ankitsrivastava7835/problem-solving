package word_count;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

public class WordCount {

	public String getKey(String word, Map<String, Integer> map) {

		for (Entry<String, Integer> entry : map.entrySet())

			if (word.equalsIgnoreCase(entry.getKey()))
				return entry.getKey();

		return null;
	}

	public void wordCount(List<String> list) {

		String word = new String();

		HashMap<String, Integer> map = new HashMap<>();

		for (int i = 0; i < list.size(); i++) {
			word = list.get(i);

			String key = getKey(word, map);

			if (Objects.nonNull(key))
				map.computeIfPresent(key, (key1, value) -> value + 1);

			else
				map.computeIfAbsent(word, value -> 1);
		}
	}

	public static void main(String[] args) {

		List<String> list = new ArrayList<>();

		list.add("apple");
		list.add("Apple");
		list.add("applE");
		list.add("Mango");
		list.add("mango");
		list.add("manGo");

		WordCount wordCount = new WordCount();
		wordCount.wordCount(list);
	}

}
